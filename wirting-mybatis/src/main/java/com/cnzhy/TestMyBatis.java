package com.cnzhy;

import com.cnzhy.bean.User;
import com.cnzhy.mapper.UserMapper;
import com.cnzhy.session.MySqlSession;

public class TestMyBatis {
	public static void main(String[] args) {
		MySqlSession sqlSession = new MySqlSession();
		UserMapper mapper = sqlSession.getMapper(UserMapper.class);
		User user = mapper.getUserById("1");
		System.out.println(user.getUsername());
	}
}
