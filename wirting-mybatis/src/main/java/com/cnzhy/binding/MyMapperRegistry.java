package com.cnzhy.binding;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.cnzhy.config.MapperStatement;
import com.cnzhy.session.MyConfiguration;

public class MyMapperRegistry {
	private static ConcurrentMap<String, MapperStatement> concurrentHashMap;

	public static ConcurrentMap<String, MapperStatement> getConcurrentHashmap() {
		return concurrentHashMap;
	}

	public static void setConcurrentHashmap(String key, MapperStatement mapperBean) {
		concurrentHashMap.put(key, mapperBean);
	}

	public static MapperStatement getMapperBean(String key) throws Exception {
		if(concurrentHashMap != null){
			return concurrentHashMap.get(key);
		}
		synchronized (MyMapperRegistry.class) {
			if(concurrentHashMap == null){
				concurrentHashMap = new ConcurrentHashMap<String, MapperStatement>();
			}
			new MyConfiguration().registryMappers("mapper");
			if(concurrentHashMap != null){
				return concurrentHashMap.get(key);
			}
		}
		return null;
	}
}
