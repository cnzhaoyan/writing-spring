package com.cnzhy.config;

import java.util.List;

public class MapperStatement {
	private String interfaceName; // 接口名
	private List<Function> functionList;

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public List<Function> getFunctionList() {
		return functionList;
	}

	public void setFunctionList(List<Function> functionList) {
		this.functionList = functionList;
	}
}
