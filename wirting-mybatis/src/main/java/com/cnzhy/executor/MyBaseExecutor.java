package com.cnzhy.executor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.dom4j.DocumentException;

import com.cnzhy.bean.User;
import com.cnzhy.session.MyConfiguration;

public class MyBaseExecutor implements Excutor {
	private MyConfiguration xmlConfiguration = new MyConfiguration();

	@SuppressWarnings("unchecked")
	@Override
	public <T> T query(String sql, Object parameter) {
		Connection connection = getConnection();
		ResultSet set = null;
		PreparedStatement pre = null;
		User u = new User();
		try {
			pre = connection.prepareStatement(sql);
			// /设置参数
			pre.setString(1, parameter.toString());
			set = pre.executeQuery();
			// 遍历结果集
			while (set.next()) {
				u.setId(set.getString(1));
				u.setUsername(set.getString(2));
				u.setPassword(set.getString(3));
			}
			
		} catch (Exception e) {
			
		}
		return (T)u;
	}

	private Connection getConnection() {
		Connection connection = null;
		try {
			connection =  xmlConfiguration.build("config.xml");
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}

}
