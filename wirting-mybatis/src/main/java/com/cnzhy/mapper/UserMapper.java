package com.cnzhy.mapper;

import com.cnzhy.bean.User;

public interface UserMapper {
	public User getUserById(String id);
}
