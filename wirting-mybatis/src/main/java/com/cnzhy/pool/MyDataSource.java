package com.cnzhy.pool;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.sql.DataSource;

public class MyDataSource implements DataSource {
	// 链表 --- 实现栈结构
	private static ConcurrentLinkedQueue<Connection> dataSources = new ConcurrentLinkedQueue<Connection>();


	@Override
	public Connection getConnection() throws SQLException {
		// 取出连接池中一个连接
		final Connection conn = dataSources.poll();// 删除第一个连接返回
		return conn;
	}
	
	public static Connection getConnectionFromPool() throws SQLException {
		// 取出连接池中一个连接
		final Connection conn = dataSources.poll();// 删除第一个连接返回
		return conn;
	}
	
	// 将连接放回连接池
	public static  void releaseConnection(Connection conn) {
		dataSources.add(conn);
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException {
		return null;
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {

	}

	@Override
	public void setLoginTimeout(int seconds) throws SQLException {

	}

	@Override
	public int getLoginTimeout() throws SQLException {
		return 0;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return false;
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		return null;
	}
}