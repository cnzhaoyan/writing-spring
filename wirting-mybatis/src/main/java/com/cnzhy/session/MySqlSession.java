package com.cnzhy.session;

import java.lang.reflect.Proxy;

import com.cnzhy.binding.MyMapperProxy;
import com.cnzhy.executor.MyBaseExecutor;

public class MySqlSession {
	private MyBaseExecutor executor = new MyBaseExecutor();

//	private MyConfiguration myConfiguration = new MyConfiguration();

	public <T> T selectOne(String statement, Object parameter) {
		return executor.query(statement, parameter);
	}

	@SuppressWarnings("unchecked")
	public <T> T getMapper(Class<T> clazz) {
		// 动态代理调用
		return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] { clazz }, new MyMapperProxy(this));
	}
}
