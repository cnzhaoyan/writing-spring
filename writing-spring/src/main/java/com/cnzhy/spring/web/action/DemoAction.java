package com.cnzhy.spring.web.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cnzhy.spring.annotation.MyAutowired;
import com.cnzhy.spring.annotation.MyController;
import com.cnzhy.spring.annotation.MyRequestMapping;
import com.cnzhy.spring.annotation.MyRequestParamMapping;
import com.cnzhy.spring.web.service.IDemoService;

@MyController
@MyRequestMapping("/demo")
public class DemoAction {
    @MyAutowired
    private IDemoService iDemoService;

    @MyRequestMapping("/query.json")
    public void query(HttpServletRequest req, HttpServletResponse res, @MyRequestParamMapping("name") String name) throws IOException {
        String result = iDemoService.get(name);
        res.getWriter().write(result);
    }
}







