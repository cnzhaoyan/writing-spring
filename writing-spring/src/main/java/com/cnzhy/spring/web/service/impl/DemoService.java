package com.cnzhy.spring.web.service.impl;

import com.cnzhy.spring.annotation.MyService;
import com.cnzhy.spring.web.service.IDemoService;

@MyService
public class DemoService implements IDemoService {

    @Override
    public String get(String name) {
        return "My name is " + name;
    }

}
